---
# Display name
title: 

# Username (this should match the folder name)
authors:
- simon

# Is this the primary user of the site?
superuser: true

# Role/position
role: 

# Organizations/Affiliations
organizations:

name: "Simon Wörpel" 

# Short bio (displayed in user profile at end of posts)
bio: 



# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
 Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

Simon is an independent investigative data journalist, researcher and leak librarian. He is specialized in documents processing, data engineering and data analysis. He works on tools that empower computer assisted investigative reporting for collaborative teams. From 2015 to mid 2019, Simon worked at [CORRECTIV](https://correctiv.org/) , an investigative not for profit newsroom based in Germany. There he built document databases for collaborative investigations like like [“The CumEx Files”](https://cumex-files.com/) and [“Grand Theft Europe”](https://grandthefteurope.com/).



