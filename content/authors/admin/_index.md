---
# Display name
title: 

# Username (this should match the folder name)
authors:
- edgar
- simon

# Is this the primary user of the site?
superuser: true

# Role/position
role: 

# Organizations/Affiliations
organizations:

name: "Hristio Boytchev"  


# Short bio (displayed in user profile at end of posts)
bio: 


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
 Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "e@vaz.io"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---
Hristio Boytchev is the founder and project manager of Follow the Grant. He is responsible for the journalistic direction of the project. Hristio is a freelance science and health journalist, focusing on data-driven and investigative reporting. From 2015 to 2017 Hristio was a reporter for the investigative non-profit newsroom Correctiv. In 2016, he was voted third among the science journalists of the year by the jury of Medium Magazine. In 2012, he was an Arthur F. Burns Fellow at the science desk of the Washington Post. Hristio holds graduate degrees in biology and journalism.
