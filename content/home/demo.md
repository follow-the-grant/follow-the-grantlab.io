+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 15  # Order that this section will appear.

title = "Project"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  gradient_start = "white"
  gradient_end = "white"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

Conflicts of interest in research and medicine are an important issue and a worthwhile subject for investigation in science journalism. Conflicts of interest arise when researchers, medical doctors or other experts receive monetary benefits (for example for consulting companies), which in turn influence their decisions. This influence has been described in detail scientifically. For example, in medicine, [scientific studies show](https://www.propublica.org/article/doctors-who-take-company-cash-tend-to-prescribe-more-brand-name-drugs) that payments from pharmaceutical companies are associated with a change in prescribing behavior, i.e. doctors can prescribe different drugs that they would otherwise.
A comprehensive database for journalistic investigations of conflicts of interest is missing at the moment. Follow the Grant aims to close the gap. We are using conflict of interest statements from scientific articles. Many scientific journals require their authors to disclose such conflicts of interest when publishing scientific articles. We aim to enable journalists to quickly check what potential conflicts of interest a particular expert has. It can also be the foundation for investigative reporting looking at discrepancies in the disclosure of funding and conflicts of interest. [Here you can see an example of a story that looked at such discrepancies.](https://www.nytimes.com/2018/09/08/health/jose-baselga-cancer-memorial-sloan-kettering.html)

  <div class="col-md-12 col-lg-8 maillist">
          <h2 class="maillist-link"><a href="https://mailchi.mp/73e215930d56/landing-page">To stay up to date, please subscribe
 to our mailing list.</a></h2>
  </div>

